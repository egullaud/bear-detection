# Bear Detector with Deep learning

This is the readme file for the Bear Detector Project developed in [CERFACS](https://cerfacs.fr).
This project is funded by the public organization the [DREAL](http://www.occitanie.developpement-durable.gouv.fr).

## The context of the project

Reintroduction of bears in the Pyrénées has been a controversed decision and has defendors as well as offenders among them sheep owners.



## The objective of the project

The objective is to develop a device that could automatically detect the presence of a bear approching a protected area and trigger a scaring device to make him peacefully go away.

## How we are going to develop it
The project contains two aspects
 1. First, a theoritical phase, where we are going to invite Artificial Intelligence (Deep Learning)
 for the bear detection
 2. Second, we will have to connect this software to a camera that can then be installed in the fields.
