# How to install FastAI

## Setup a virtual environment


## How to create my own kernel in my jupyter notebook

I cre

The procedure is explained [here](https://docs.python.org/3/library/venv.html)

If I want to create a virtual environment called fastai in path_virtual_environment = /home/username/Python_envs/fastai

```
python3 -m venv /path_virtual_environment
```
I don't want to detail the whole procedure here but the most important it it creates a folder named by the name of the virtual environment chosen, and a folder lib in which is located the folder site-packages where you will find specific packages that you will be installing in the virtual environment, it's the whole big advantage of the virtual environment!

Then to actually be in the virtual environment the command is:

```
source /path_virtual_environment/bin/activate
```



## How to create your own kernel in your jupyter notebook

Creating a virtual environment and activating it is great, now when you type python3 in the activated virtual environment, you will be in it and have access to the so called packages installed specifically in this package, but what if I run on Jupyter notebook ?

1. First, install the module ipykernel: 

After activating your virtual environment,

```
pip install ipykernel
```

2. Add this environment to jupyter

3. Check that you have the environment in jupyter

<p align="center"> 
    <img src="./venv_kernel.png" width=300>
</p>
